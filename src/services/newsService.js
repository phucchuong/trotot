const News = require("../models/news");
const NEWS_STATUS = require("../utils/constant").NEWS_STATUS;

const insertNews = async newsData => {
  const news = new News(newsData);
  return await news.save();
};

const getNewsById = async id => {
  return await News.findById(id);
};

const getTotalPage = async (pageSize, query) => {
  const count = await News.countDocuments(query);
  return Math.ceil(count / pageSize);
};

const getPageNews = async (query, page, pageSize) => {
  page = page > 0 ? page : 1;
  const totalPage = await getTotalPage(pageSize, query);
  const data = await query
    .sort({ _id: -1 })
    .skip((page - 1) * pageSize)
    .limit(pageSize);
  return { page, pageSize, totalPage, data };
};

const filterNews = (query, city, town, search)=>{
  const regrex = `${search}`;
  if(city && town){
    return query.where('address.province_city').equals(city)
    .where('address.district').equals(town)
    .where('title', new RegExp(regrex))
  }else if(city){
    return query.where('address.province_city').equals(city)
    .where('title', new RegExp(regrex))
  }else if(town){
    return query.where('address.district').equals(town)
    .where('title', new RegExp(regrex))
  }
  return query.where('title', new RegExp(regrex));
}

const getNewsByStatus = async (status, page = 1, pageSize = 10, city="", town="", search="") => {
  let query;
  switch (status) {
    case NEWS_STATUS.ACTIVE:
      query = News.find({ status: NEWS_STATUS.ACTIVE });
      break;
    case NEWS_STATUS.INACTIVE:
      query = News.find({ status: NEWS_STATUS.INACTIVE });
      break;
    case NEWS_STATUS.RENT:
      query = News.find({ status: NEWS_STATUS.RENT });
      break;
    default:
      query = News.find();
  }

  query = filterNews(query,city, town, search);

  return await getPageNews(query, page, pageSize);
};

const stringFilter = async (field, filterKey, page, pageSize = 10) => {
  const query = News.find({ field: filterKey });
  return await getPageNews(query, page, pageSize);
};

const deleteNewsById = async id => {
  return await News.findByIdAndDelete(id);
};

const deleteNewsByIdNewsAndAccout = async (id, id_account) => {
  return await News.findOneAndDelete({ _id: id, id_account });
};

const getNewsByAccountId = async id_account => {
  return await News.find({ id_account });
};

const changeNewsStatus = async (id, status) => {
  return await News.findByIdAndUpdate(id, { status });
};

const editNews = async (id, id_account, news) => {
  return await News.findOneAndUpdate({ _id: id, id_account }, news,{new:true});
};

const getNewsByAccountIdAndStatus = async(id_account, status)=>{
  return await News.find({id_account,status});
}

const countTotalNews = async ()=>{
  return await News.countDocuments();
}

const countNewsByStatus = async(status)=>{
  return await News.countDocuments({status});
}

module.exports = {
  insertNews,
  getNewsById,
  deleteNewsById,
  getNewsByAccountId,
  stringFilter,
  getNewsByStatus,
  changeNewsStatus,
  editNews,
  deleteNewsByIdNewsAndAccout,
  getNewsByAccountIdAndStatus,
  countNewsByStatus,
  countTotalNews
};
