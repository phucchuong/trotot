const Message = require("../models/message");

const insertMessage = async content => {
  const message = new Message(content);
  return await message.save();
};

const getMessageById = async id => {
  return await Message.findById(id);
};

const getMessageArray = async arrayId => {
  const result = arrayId.map(id => {
    return await getMessageById(id);
  });
  return result;
};

const deleteMessageById = async id=>{
    return await Message.findByIdAndDelete(id);
}

const deleteMessageArray = async arrayId =>{
    const result = arrayId.map(id => {
        return await deleteMessageById(id);
      });
    return result;
}

module.exports = { insertMessage, getMessageById,getMessageArray, deleteMessageById, deleteMessageArray};
