const Address = require("../models/address");

const getAddressByProvince = async province => {
  return await Address.findOne({ province_city: province });
};

const getAddressById = async id => {
  return await Address.findById(id);
};

const getListProvince = async () => {
  return await Address.find().select("province_city");
};

module.exports = {
  insertAddress,
  getAddressByProvince,
  getAddressById,
  getListProvince
};
