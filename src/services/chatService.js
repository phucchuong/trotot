const Chat = require("../models/chat");

const insertChat = async chat => {
  // chat{from,to,id}
};
const getSentChat = async id_account => {
  return await Chat.find({ from: id_account });
};
const getReceivedChat = async id_account => {
  return await Chat.find({ to: id_account });
};

module.exports = { insertChat, getReceivedChat, getSentChat };
