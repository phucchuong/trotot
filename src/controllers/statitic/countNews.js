const {
  countNewsByStatus,
  countTotalNews
} = require("../../services/newsService");
const {
  InternalServerError,
} = require("../../utils/ResponseHelper");
const { ACTIVE, INACTIVE, RENT } = require("../../utils/constant").NEWS_STATUS;

const get = async (req, res) => {
  try {
    const total = await countTotalNews();
    const activeNews = await countNewsByStatus(ACTIVE);
    const inactiveNews = await countNewsByStatus(INACTIVE);
    const rentNews = await countNewsByStatus(RENT);
    res.send({ total, activeNews, inactiveNews, rentNews });
  } catch (error) {
    console.log(error);
    InternalServerError(res);
  }
};

module.exports = get;
