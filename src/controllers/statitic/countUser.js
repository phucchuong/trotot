const {
    countTotalAccount
  } = require("../../services/accountService");
  const {
    InternalServerError,
  } = require("../../utils/ResponseHelper");
  
  const get = async (req, res) => {
    try {
      const total = await countTotalAccount();
      res.send({ total});
    } catch (error) {
      console.log(error);
      InternalServerError(res);
    }
  };
  
  module.exports = get;
  