const router = require("express").Router({ mergeParams: true });
const {requiredAdmin} = require("../../middlewares/auth");

router.get("/accounts", requiredAdmin, require("./countUser"));
router.get("/news", requiredAdmin, require("./countNews"));

module.exports = router;