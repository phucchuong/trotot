const router = require("express").Router({ mergeParams: true });
const {requiredLogin} = require("../../middlewares/auth");

router.get("/:id", require("./getUserInfoById"));
router.get("/:id/news", require("../news/getNewsByAccoutIdAndStatus"));
router.get("/favourites", requiredLogin, require("./getFavouriteNews"));
router.put("/:id", requiredLogin, require("./updateInfo"));
router.patch("/", requiredLogin, require("./changeAvatar"));
router.patch("/:id", requiredLogin, require("./markFavourite"));
//router.delete("/:id", requiredLogin, require("./removeFavourite"));

module.exports = router;