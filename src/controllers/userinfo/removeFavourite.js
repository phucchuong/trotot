const {removeFavourite} = require("../../services/userInforService");
const {InternalServerError, BadRequest} = require("../../utils/ResponseHelper");
const {isValidObjectId} = require("../../utils/validatorRequest")

const favourite = async(req,res)=>{
    const {id} = req.params;
    const {id:id_account} = req.decoded;
    if(!id || !isValidObjectId(id)) return BadRequest(res," Invalid news_id");
    try{
        const result = await removeFavourite(id_account,id);
        res.send(result);
    }catch(e){
        console.log(e);
        InternalServerError(res);
    }
}

module.exports = favourite;