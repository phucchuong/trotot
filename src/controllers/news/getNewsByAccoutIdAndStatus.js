const { getNewsByAccountIdAndStatus } = require("../../services/newsService");
const {
  InternalServerError,
  BadRequest
} = require("../../utils/ResponseHelper");

const get = async (req, res) => {
  const { page, pageSize ,status} = req.query;
  const {id: account_id} = req.params;
  try {
    const result = await getNewsByAccountIdAndStatus(account_id, status);
    res.send({data:result});
  } catch (error) {
    console.log(error)
    InternalServerError(res);
  }
};

module.exports = get;
