const { editNews } = require("../../services/newsService");
const {NEWS_STATUS} = require("../../utils/constant");
const {
    InternalServerError,
    BadRequest,
    NotFound
  } = require("../../utils/ResponseHelper");

const edit = async (req, res) => {
  const { id } = req.params;
  const { id: idAccount } = req.decoded;
  try {
    const result = await editNews(id, idAccount, {status:NEWS_STATUS.RENT});
    if(!result) return NotFound(res,id+" is not found");
    res.send(result);
  } catch (error) {
    InternalServerError(res);
  }
};

module.exports = edit;
