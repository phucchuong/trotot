const { getNewsByStatus } = require("../../services/newsService");
const {
  InternalServerError,
  BadRequest
} = require("../../utils/ResponseHelper");
const { NEWS_STATUS } = require("../../utils/constant");

const get = async (req, res) => {
  let { page, pageSize , city, town, search} = req.query;
  page = parseInt(page) || 1;
  pageSize = parseInt(pageSize) || 20;
  console.log(page, pageSize, city, town, search)
  //if (page > totalPage) return BadRequest(res, "Wrong page number!");
  try {
    const result = await getNewsByStatus(NEWS_STATUS.ACTIVE, page, pageSize, city, town, search);
    res.send(result);
  } catch (error) {
    console.log(error)
    InternalServerError(res);
  }
};

module.exports = get;
