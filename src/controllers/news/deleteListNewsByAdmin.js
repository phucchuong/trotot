const { deleteNewsById } = require("../../services/newsService");
const {
    InternalServerError,
    BadRequest,
    NotFound,
    Ok
  } = require("../../utils/ResponseHelper");

const edit = async (req, res) => {
  const { ids } = req.body;
  if(!ids) return BadRequest(res)
  console.log(ids, typeof ids)
  try {
    ids.map(async(id)=>{
        await deleteNewsById(id);
    })
    Ok(res, "Deleted");
  } catch (error) {
      console.log(error)
    InternalServerError(res);
  }
};

module.exports = edit;
