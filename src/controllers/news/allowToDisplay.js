const {changeNewsStatus} = require("../../services/newsService");
const {
    InternalServerError,
    BadRequest
} = require("../../utils/ResponseHelper");

const allowToDisplay = async(req,res)=>{    
    try{
        const {id} = req.params;
        const {status} = req.body;
        if(!id || !status) return BadRequest(res);
        const result = await changeNewsStatus(id,status);
        res.send(result);
    }catch(e){
        console.log(e);
        InternalServerError(res);
    }
}

module.exports = allowToDisplay