const router = require("express").Router();
const adminRouter = require("express").Router();
const { requiredLogin ,requiredAdmin } = require("../../middlewares/auth");

router.get("/", require("./getActiveNews"));
router.get("/:id", require("./getNewsById"));
router.post("/",requiredLogin, require("./postNews"));
router.put("/:id",requiredLogin, require("./editNews"));
router.patch("/:id",requiredLogin, require("./markRent"));
router.delete("/:id",requiredLogin, require("./deleteNews"));

//admin
adminRouter.patch("/:id",requiredAdmin, require("./allowToDisplay"));
adminRouter.get("/",requiredAdmin, require("./getNewsByStatus"));
adminRouter.delete("/:id",requiredAdmin, require("./deleteNewsByAdmin"));
adminRouter.delete("/",requiredAdmin, require("./deleteListNewsByAdmin"));



module.exports = {router,adminRouter};
