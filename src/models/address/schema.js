const Schema = require("mongoose").Schema;

const accountSchema = new Schema({
  province_city: {
    type: String,
    unique: true
  },
  district: {
    type: Array
  }
});

module.exports = accountSchema;
