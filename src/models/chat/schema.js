const Schema = require("mongoose").Schema;

const chatSchema = new Schema({
  from: { type: Schema.Types.ObjectId },
  to: { type: Schema.Types.ObjectId },
  id_message: { type: Array }
});

module.exports = chatSchema;
