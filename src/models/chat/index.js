const mongoose = require("mongoose");

module.exports = mongoose.model("Chat", require("./schema"));