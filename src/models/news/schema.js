const Schema = require("mongoose").Schema;

const newsSchema = new Schema({
  id_account: { type: Schema.Types.ObjectId },
  title: { type: String ,required:true},
  status: {type:Number, default: 1}, // 1.hide, 2.display, 3.rented
  area: Number, // dien tich
  location: { longtitude: Number, latitude: Number },
  address: {
    province_city: String,
    district: String,
    street: String // so nha, duong
  },
  price: Number,
  catagory: {
    isForMale: Boolean,
    roomType: Number // 1.tro, 2.nha nguyen can, 3.phong trong nha nguyen can
  },
  description: String,
  image: { type: Array },
  createDay: { type: Date, default: Date.now }
});

module.exports = newsSchema;
