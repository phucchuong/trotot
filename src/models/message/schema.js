const Schema = require("mongoose").Schema;

const messageSchema = new Schema({
  content: { type: String },
  createdDay: { type: Date, default: Date.now }
});

module.exports = messageSchema;
